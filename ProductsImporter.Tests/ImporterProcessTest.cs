using FluentAssertions;
using Moq;
using ProductsImporter.Core.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace ProductsImporter.Tests
{
    public class ImporterProcessTest
    {

        //private readonly Mock<ISqlRepositoryService> _mockRepositoryService;
        private readonly Mock<IOutputTypeStrategy> _mockStrategyService;
        private readonly ImporterService _sut;

        public ImporterProcessTest()
        {            
            //_mockRepositoryService = new Mock<ISqlRepositoryService>();            
            _mockStrategyService = new Mock<IOutputTypeStrategy>(); 
            
            _sut = new ImporterService(_mockStrategyService.Object);            
        }

        [Fact]
        public async Task Process_ValidPath_ExpectedTrue()
        {
            //Arrange           
            var path = "validPath";
            var site = Core.Enums.Sites.Capterra;

            _mockStrategyService.Setup(_ => _.Process(It.IsAny<string>(), It.IsAny<Core.Enums.Sites>())).ReturnsAsync(true);

            //Act
            var result = await _sut.Process(path, site);

            //Assert
            result.Should().BeTrue();             
            _mockStrategyService.Verify(_ => _.Process(It.IsAny<string>(), It.IsAny<Core.Enums.Sites>()), Times.Once);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task Process_NullOrEmptyPath_ExpectedArgumentException(string path)
        {
            //Arrange           
            var site = Core.Enums.Sites.Capterra;

            _mockStrategyService.Setup(_ => _.Process(It.IsAny<string>(), It.IsAny<Core.Enums.Sites>())).ReturnsAsync(true);

            //Act
            Func<Task> result = () => _sut.Process(path, site);

            //Assert
            await result.Should().ThrowAsync<ArgumentException>();            
            _mockStrategyService.Verify(_ => _.Process(It.IsAny<string>(), It.IsAny<Core.Enums.Sites>()), Times.Never);

        }

        [Fact]
        public async Task Process_ServiceDown_ExpectedIoException()
        {
            //Arrange           
            var path = "validPath";
            var site = Core.Enums.Sites.Capterra;

            _mockStrategyService.Setup(_ => _.Process(It.IsAny<string>(), It.IsAny<Core.Enums.Sites>())).Throws<IOException>();

            //Act
            Func<Task> result = () => _sut.Process(path, site);

            //Assert
            await result
              .Should().ThrowAsync<IOException>();                   
            _mockStrategyService.Verify(_ => _.Process(It.IsAny<string>(), It.IsAny<Core.Enums.Sites>()), Times.Once);

        }
    }
}
