﻿
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProductsImporter;
using ProductsImporter.Core.Enums;
using ProductsImporter.Core.Extensions;
using ProductsImporter.Core.Interfaces;
using ProductsImporter.Infra.Data;
using ProductsImporter.Infra.Services;


using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services.AddScoped<IOutputTypeStrategy, JsonOutputService>()
                .AddScoped<IOutputTypeStrategy, YamlOutputService>()
                .AddScoped<IOutputTypeStrategy, CsvOutputService>()
                .AddScoped<ISqlRepositoryService, SqlRepositoryService>())
    .Build();


await Run(host.Services);

await host.RunAsync();
 

async Task Run(IServiceProvider services)
{
    using IServiceScope serviceScope = services.CreateScope();
    IServiceProvider provider = serviceScope.ServiceProvider;

    try
    {
        Console.WriteLine("*** Starting process  ***");
        Console.WriteLine("*** ***************** ***");
        Console.WriteLine("*** ***************** ***");

        const string searchPattern = "*.*"; //in real life this goes in app settings file
        const string directory = "C:\\Dev\\ProductsImporter\\ProductsImporter\\Products\\"; //in real life this goes in app settings file

        //I'm using the two mock files provided directly, but they should come as part of a service
        var filesInDirectory = Directory.EnumerateFiles(directory, searchPattern).ToList();
        //TODO: get also the third provider file from the url and process all three together

        var strategyInstances = provider.GetRequiredService<IEnumerable<IOutputTypeStrategy>>();

        foreach (var path in filesInDirectory)
        {
            var siteName = Path.GetFileName(path).Split(".")[0];
            var ext = Path.GetExtension(path);
            var site = (Sites)Enum.Parse(typeof(Sites), siteName.ToPascalCase());

            switch (ext)
            {
                case ".yaml":

                    var yamlInstance = strategyInstances.First(e => e.GetType() == typeof(YamlOutputService));
                    
                    var yamlProcessor = new ImporterService(yamlInstance);

                    await yamlProcessor.Process(path, site);
                    break;
                case ".json":
                    var jsonInstance = strategyInstances.First(e => e.GetType() == typeof(JsonOutputService));

                    var jsonProcessor = new ImporterService(jsonInstance);

                    await jsonProcessor.Process(path, site);
                    break;
                case ".csv":
                    var csvInstance = strategyInstances.First(e => e.GetType() == typeof(CsvOutputService));

                    var csvProcessor = new ImporterService(csvInstance);

                    await csvProcessor.Process(path, site);
                    break;
            }
        }
        
        Console.WriteLine("*** Process has ended ***");
        Console.WriteLine("*** ***************** ***");
        Console.WriteLine("*** ***************** ***");
    }
    catch (IOException ex)
    {
        Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}








