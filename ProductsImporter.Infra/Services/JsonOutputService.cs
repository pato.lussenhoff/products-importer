﻿using ProductsImporter.Core.Enums;
using ProductsImporter.Core.Extensions;
using ProductsImporter.Core.Interfaces;
using ProductsImporter.Core.Models;

namespace ProductsImporter.Infra.Services
{
    public class JsonOutputService : IOutputTypeStrategy
    {

        public JsonOutputService()
        {
            //Here we could inject a repository service to persist the data in SQL so we can use it to process the products
            //Then we can extend the repository to persist the data in MongoDB and use that one to do the switch
        }

        public async Task<bool> Process(string path, Sites site)
        {

            var siteMapped = site.ToJsonModel(File.ReadAllText(path));
  
            var list = siteMapped?.GetType()?.GetProperty("Products")?.GetValue(siteMapped, null) as IList<SoftwareAdviceProduct>;

            list.PrintList(Path.GetFileName(path));

            //_repositoryService.SaveProducts(list);
            
            await Task.Delay(100);

            return true;
        }
    }
}
