﻿
namespace ProductsImporter.Core.Models
{
    public class SoftwareAdvice : AbstractSite
    {
        public List<SoftwareAdviceProduct>? Products { get; set; }
    }

    public class SoftwareAdviceProduct
    {
        public List<string>? Categories { get; set; }
        public string? Twitter { get; set; }
        public string? Title { get; set; }
    }
}
