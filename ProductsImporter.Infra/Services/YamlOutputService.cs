﻿using ProductsImporter.Core.Enums;
using ProductsImporter.Core.Extensions;
using ProductsImporter.Core.Interfaces;

namespace ProductsImporter.Infra.Services
{
    public class YamlOutputService: IOutputTypeStrategy
    {
        public YamlOutputService()
        {
            //here we could inject a repository service to persist the data in SQL so we can use it to process the products
            //then we can extend the repository to persist the data in MongoDB and use that one to do the switch
        }

        public async Task<bool> Process(string path, Sites site)
        {            

            var products = site.ToYamlModel(File.ReadAllText(path)).ToList();

            products.PrintList(Path.GetFileName(path));

            //_repositoryService.SaveProducts(list);

            await Task.Delay(100);

            return true;
        }
    }
}
