﻿
namespace ProductsImporter.Core.Models
{
    public class Capterra : AbstractSite
    {
        public string? Tags { get; set; }
        public string? Name { get; set; }
        public string? Twitter { get; set; }
    }

}
