﻿using ProductsImporter.Core.Enums;
using ProductsImporter.Core.Interfaces;

namespace ProductsImporter
{

    public class ImporterService : IImporterService
    {
        private readonly IOutputTypeStrategy _typeStrategy;

        public ImporterService(IOutputTypeStrategy typeStrategy) => (_typeStrategy) = (typeStrategy);

        public async Task<bool> Process(string path, Sites site)
        {

            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("path cannot be null");

            var result = await _typeStrategy.Process(path, site);

            return result;
        }

    }
}
