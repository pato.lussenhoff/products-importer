﻿using ProductsImporter.Core.Enums;

namespace ProductsImporter.Core.Interfaces
{
    public interface IImporterService
    {
        Task<bool> Process(string path, Sites site);
    }
}