﻿using Newtonsoft.Json;
using ProductsImporter.Core.Enums;
using ProductsImporter.Core.Models;
using YamlDotNet.Serialization.NamingConventions;

namespace ProductsImporter.Core.Extensions
{

    public static class SiteHelper
    {

        public static IEnumerable<AbstractSite> ToYamlModel(this Sites site, string file)
        {
            var deserializer = new YamlDotNet.Serialization.DeserializerBuilder()
            .WithNamingConvention(CamelCaseNamingConvention.Instance)
            .Build();

            return site switch
            {
                Sites.Capterra => deserializer.Deserialize<List<Capterra>>(file),
                _ => throw new ArgumentException("No site associated to the file.")
            };
        }

        public static AbstractSite? ToJsonModel(this Sites sites, string file) => sites switch
        {
            Sites.Softwareadvice => JsonConvert.DeserializeObject<SoftwareAdvice>(file),            
            _ => throw new ArgumentException("No site associated to the file.")
        };

    }
}
