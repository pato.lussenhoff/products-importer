# Products-Importer

## Steps to run the solution

- [ ] [Clone the repo]
- [ ] [Build the solution](Required packages will be restored such us YamlDotNet among others)
- [ ] [Run the solution]
- [ ] [Run unit tests from the Test Explorer]

## Solution
I designed the solution using clean architecture. 
-Infrastructure layer that contains all those services implementation to connect to external sources (such us DBs, APIs, etc).
-Core layer that contains those models, helpers and interfaces of those implementations.
-Test layer that contains all the unit tests files for the solution.

I leant towards to strategy pattern approach which we have different ways to process a file depending on the extension.

## Unit Testing
I have a lot of experience on writing unit testing, even delivered a course for new hires at the company. I just provided a example on how to test a class. 
As assertion library I used one called FluentAssertions that I think it has more readability compared to the one provided by the framework as it allows you to more naturally specify the expected outcome for a specific unit test.
I also used Moq library for mocking dependencies as part of the arrange step.

## DB Exercise
As I was running into issues because of the company policies for installing sql server management studio locally, I decided to create a SQL Database in Azure and replicated the database script provided to start working on the requested queries. These are uploaded in this repo as txt file.

