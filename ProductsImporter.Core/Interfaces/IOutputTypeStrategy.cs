﻿using ProductsImporter.Core.Enums;

namespace ProductsImporter.Core.Interfaces
{
    public interface IOutputTypeStrategy
    {
        Task<bool> Process(string path, Sites site);
    }
}
