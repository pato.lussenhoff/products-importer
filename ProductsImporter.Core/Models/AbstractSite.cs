﻿
namespace ProductsImporter.Core.Models
{
    public abstract class AbstractSite
    {
        public string Id { get; set; }
        public DateTime ProcessDate { get; set; }

    }
}
