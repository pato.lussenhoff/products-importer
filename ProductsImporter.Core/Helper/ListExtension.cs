﻿
namespace ProductsImporter.Core.Extensions
{
    public static class ListExtension
    {
        public static void PrintList<T>(this IList<T> list, string fileName)
        {
            var propertyNames = list[0]?.GetType().GetProperties().Select(p => p.Name);

            Console.WriteLine($"***** importing { fileName } *****");

            foreach (var product in list)
            {
                string message = "importing:";

                foreach (var prop in propertyNames)
                {                    
                    var propValue = product?.GetType()?.GetProperty(prop)?.GetValue(product, null);

                    if (propValue is IEnumerable<object>)
                    {
                        string result = string.Join(", ", propValue as IEnumerable<object>);
                        message += propValue is not null ? $" {prop}: '{result}';" : string.Empty;
                    } else
                    {
                        message += propValue is not null ? $" {prop}: '{propValue}';" : string.Empty;
                    }
           
                }

                Console.WriteLine(message);
            }
        }
    }
}
